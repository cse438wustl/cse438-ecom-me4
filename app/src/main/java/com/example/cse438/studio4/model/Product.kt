package com.example.cse438.studio4.model

import java.io.Serializable
import kotlin.collections.ArrayList

class Product(): Serializable {
    // Identifiers
    private var id: String = ""

    // Manufacturer Details
    private var manufacturer: String = ""
    private var model: String = ""
    private var brand: String = ""

    // Category Information
    private var cat_id: Int = 0
    private var category: String = ""

    // Dimensions and Weight
    private var weight: Double = 0.0
    private var length: Double = 0.0
    private var width: Double = 0.0
    private var height: Double = 0.0

    // Images
    private var images_total: Int = 0
    private var imageSem3UrlStrings: ArrayList<String> = ArrayList()

    // Pricing
    private var price: Double = 0.0
    private var price_currency: String = ""

    // Product Information
    private var name: String = ""
    private var description: String = ""
    private var color: String = ""
    private var features: HashMap<String, String> = HashMap()
    private var is_new: Int = 0

    // Important Codes
    private var upc: Long = 0                           // Universal Product Number
    private var ean: Long = 0                           // European  Article Number
    private var mpn: String = ""                        // Manufacturer Part Number
    private var gtins: ArrayList<Long> = ArrayList()    // Global Trade Item Number

    // Key Dates
    private var created_at: Long = 0
    private var updated_at: Long = 0

    // Unimportant Data
    private var variation_secondaryids: ArrayList<String> = ArrayList()
    private var sem3_id: String = ""
    private var sitedetails: ArrayList<SiteDetail> = ArrayList()
    private var geo: ArrayList<String> = ArrayList()
    private var messages: ArrayList<String> = ArrayList()

    private var imageCloudUrlStrings: ArrayList<String> = ArrayList()
    var isFavorite: Boolean = false

    var reviews = ArrayList<Review>()

    constructor(
        id: String,
        manufacturer: String,
        model: String,
        brand: String,
        cat_id: Int,
        category: String,
        weight: Double,
        length: Double,
        width: Double,
        height: Double,
        images_total: Int,
        imageSem3UrlStrings: ArrayList<String>,
        price: Double,
        price_currency: String,
        name: String,
        description: String,
        color: String,
        features: HashMap<String, String>,
        is_new: Int,
        upc: Long,
        ean: Long,
        mpn: String,
        gtins: ArrayList<Long>,
        created_at: Long,
        updated_at: Long,
        variation_secondaryids: ArrayList<String>,
        sem3_id: String,
        sitedetails: ArrayList<SiteDetail>,
        geo: ArrayList<String>,
        messages: ArrayList<String>
    ) : this() {
        this.id = id
        this.manufacturer = manufacturer
        this.model = model
        this.brand = brand
        this.cat_id = cat_id
        this.category = category
        this.weight = weight
        this.length = length
        this.width = width
        this.height = height
        this.images_total = images_total
        this.imageSem3UrlStrings = imageSem3UrlStrings
        this.price = price
        this.price_currency = price_currency
        this.name = name
        this.description = description
        this.color = color
        this.features = features
        this.is_new = is_new
        this.upc = upc
        this.ean = ean
        this.mpn = mpn
        this.gtins = gtins
        this.created_at = created_at
        this.updated_at = updated_at
        this.variation_secondaryids = variation_secondaryids
        this.sem3_id = sem3_id
        this.sitedetails = sitedetails
        this.geo = geo
        this.messages = messages

        convertSem3UrlsToCloud()
    }

    private fun convertSem3UrlsToCloud() {
        val sem3UrlPreface = "http://sem3-idn.s3-website-us-east-1.amazonaws.com/"
        val cloudUrlPreface = "https://res.cloudinary.com/du3z7sadc/image/upload/v1/Apple_Products/"

        for (index in this.imageSem3UrlStrings) {
            var urlPost = index.substring(sem3UrlPreface.length until index.length)
            urlPost = urlPost.replace(',', '_')
            imageCloudUrlStrings.add(cloudUrlPreface + urlPost)
        }
    }

    //                        //
    // ------ Getters ------- //
    //                        //

    fun getId(): String {
        return this.id
    }

    fun getManufacturer(): String {
        return this.manufacturer
    }

    fun getModel(): String {
        return this.model
    }

    fun getBrand(): String {
        return this.brand
    }

    fun getCategoryId(): Int {
        return this.cat_id
    }

    fun getCategory(): String {
        return this.category
    }

    fun getWeight(): Double {
        return this.weight
    }

    fun getLength(): Double {
        return this.length
    }

    fun getWidth(): Double {
        return this.width
    }

    fun getHeight(): Double {
        return this.height
    }

    fun getImageCount(): Int {
        return this.images_total
    }

    fun getImages(): ArrayList<String> {
        return this.imageCloudUrlStrings
    }

    fun getSem3Images(): ArrayList<String> {
        return this.imageSem3UrlStrings
    }

    fun getPrice(): Double {
        return this.price
    }

    fun getPriceCurrency(): String {
        return this.price_currency
    }

    fun getProductName(): String {
        return this.name
    }

    fun getProductDescription(): String {
        return this.description
    }

    fun getProductColor(): String {
        return this.color
    }

    fun getProductFeatures(): HashMap<String, String> {
        return this.features
    }

    fun getIsNew(): Boolean {
        return this.is_new > 0
    }

    fun getUniversalProductNumber(): Long {
        return this.upc
    }

    fun getEuropeanArticleNumber(): Long {
        return this.ean
    }

    fun getManufacturerPartNumber(): String {
        return this.mpn
    }

    fun getGlobalTradeItemNumbers(): ArrayList<Long> {
        return this.gtins
    }

    fun getCreatedDate(): Long {
        return this.created_at
    }

    fun getUpdatedDate(): Long {
        return this.updated_at
    }

    fun getSem3Id(): String {
        return this.sem3_id
    }

    fun getVariationSecondaryIds(): ArrayList<String> {
        return this.variation_secondaryids
    }

    fun getGeography(): ArrayList<String> {
        return this.geo
    }

    fun getMessages(): ArrayList<String> {
        return this.messages
    }

    fun getSiteDetails(): ArrayList<SiteDetail> {
        return this.sitedetails
    }
}